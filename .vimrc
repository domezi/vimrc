"""" START Vundle Configuration 

" Disable file type for vundle
filetype off                        " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim~
call vundle#begin()
" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" HTML/PHP Support
"Plugin 'captbaritone/better-indent-support-for-php-with-html'
Plugin 'tpope/vim-surround'
Plugin 'alvan/vim-closetag'
"Plugin 'jiangmiao/auto-pairs'      " when type ( auto write )
"Plugin 'StanAngeloff/php.vim'

" Vue Support
"Plugin 'posva/vim-vue'

" Utility
"Plugin 'scrooloose/nerdtree'
Plugin 'ctrlpvim/ctrlp.vim'
"Plugin 'SirVer/ultisnips'

" Theme / Interface
Plugin 'NLKNguyen/papercolor-theme' 

call vundle#end()                   " required

" Config plugins
set t_Co=256                        " This is may or may not needed.
set background=light                " may also use dark
set background=dark                " may also use dark
colorscheme PaperColor      " for debugging disabled

" Closetag
let g:closetag_filenames = '*.html,*.xhtml,*.phtml,*.php,*.js,*.vue'
let g:closetag_xhtml_filenames = '*.xhtml,*.jsx,*.tsx,*.ts'
let g:closetag_filetypes = 'php,html,xhtml,phtml,vue'
let g:closetag_xhtml_filetypes = 'xhtml,jsx,tsx'
let g:closetag_regions = {
    \ 'typescript.tsx': 'jsxRegion,tsxRegion',
    \ 'javascript.jsx': 'jsxRegion',
    \ }
let g:closetag_shortcut = '>'
let g:closetag_close_shortcut = '<leader>>'         " don't autoclose, just type the > sign

" Ultisnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

" CtrlP config
let g:ctrlp_custom_ignore = {
	\   'dir' : '\.git$\|build$\|bower_components\|node_modules\|vendor\|dist\|target' ,
	\ 	'file' : '\v\.(exe|dll|lib)$'
	\ }

let g:ctrlp_match_window = 'bottom,order:ttb,min:1,max:5,results:5'

" Nerdtree
let NERDTreeHijackNetrw = 0

" vim-vue 
let g:vue_pre_processors = []

" php.vim, put this function at the very end of your vimrc file.

function! PhpSyntaxOverride()
  " Put snippet overrides in this function.
  hi! link phpDocTags phpDefine
  hi! link phpDocParam phpType
endfunction

augroup phpSyntaxOverride
  autocmd!
  autocmd FileType php call PhpSyntaxOverride()
augroup END

" Vundle ?
filetype plugin indent on                  " required

"""" END Vundle Configuration 


"""""""""""""""""""""""""""""""""""""
" Configuration Section
"""""""""""""""""""""""""""""""""""""
set mouse=a
set nocompatible
syntax on
set nowrap
set encoding=utf8
let mapleader = ',' 					  	"The default is \, but a comma is much better.

" Show linenumbers
set number
set ruler

" Set Proper Tabs
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab

" Always display the status line
set laststatus=2

"-------------Split Management--------------"
set splitbelow 								"Make splits default to below...
set splitright								"And to the right. This feels more natural.

"We'll set simpler mappings to switch between splits.
nmap <C-J> <C-W><C-J>
nmap <C-K> <C-W><C-K>
nmap <C-H> <C-W><C-H>
nmap <C-L> <C-W><C-L>


autocmd FileType html,vue,php,css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown,vue,jsx,tsx,php setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript,vue,jsx,tsx setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Visuals
set guioptions-=l                               "Disable Gui scrollbars.
set guioptions-=L
set guioptions-=r
set guioptions-=R
set guifont=Fira\ Code:h17						"Set the default font family and size.
hi vertsplit guifg=bg guibg=bg                  "Get rid of ugly split borders.
 

"-------------Search--------------"
set hlsearch								"Highlight all matched terms.
set incsearch								"Incrementally highlight, as we type.


"""""""""""""""""""""""""""""""""""""
" Mappings configurationn
"""""""""""""""""""""""""""""""""""""

"Make it easy to edit the Vimrc file.
nmap <Leader>ev :tabedit $MYVIMRC<cr>
nmap <Leader>es :e ~/.vim/snippets/

"Add simple highlight removal.
nmap <Leader><space> :nohlsearch<cr>

map <C-n> :NERDTreeToggle<CR>
nmap ,n :NERDTreeFind<CR>

noremap <silent> <C-S>          :update<CR>
vnoremap <silent> <C-S>         <C-C>:update<CR>
inoremap <silent> <C-S>         <C-O>:update<CR>

" append ; to line and go into next line
noremap <Leader>;       A;<CR>
 
"-------------Laravel-Specific--------------"
nmap <Leader><Leader>am :!php artisan make:
" nmap <Leader><Leader>m :CtrlP<cr>app/
" nmap <Leader><Leader>c :e app/Http/Controllers/<cr>
nmap <Leader><Leader>lv :CtrlP<cr>resources/views/
nmap <Leader><Leader>la :CtrlP<cr>app/
nmap <Leader><Leader>lc :CtrlP<cr>app/Http/Controllers/
nmap <Leader><Leader>lm :CtrlP<cr>database/migrations/
nmap <Leader><Leader>lf :CtrlP<cr>database/factories/
nmap <Leader><Leader>lj :CtrlP<cr>resources/js/

"-------------GIT-Specific--------------"
" nmap <Leader><Leader>ga :!git add .<cr>
nmap <Leader><Leader>gc :!git add .<cr>:!git commit -m "
nmap <Leader><Leader>gp :!git push<cr>

"-------------NPM-Specific--------------"
nmap <Leader><Leader>ni :!npm install --save 


"Sort PHP use statements
"http://stackoverflow.com/questions/11531073/how-do-you-sort-a-range-of-lines-by-length
vmap <Leader>su ! awk '{ print length(), $0 \| "sort -n \| cut -d\\  -f2-" }'<cr>





"-------------Auto-Commands--------------"
"Automatically source the Vimrc file on save.

augroup autosourcing
    autocmd!
    autocmd BufWritePost .vimrc source %
augroup END


"-------------Tips and Reminders--------------"
" - Press 'zz' to instantly center the line where the cursor is located.
